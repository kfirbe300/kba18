<?php

namespace app\models;

use Yii;
use yii\widgets\ActiveForm;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Subcontractor;
use app\models\User;
use app\models\Type;
use app\models\Task;
use app\models\Projectpartner;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\UnauthorizedHttpException;

class Project extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'project';
    }

    public function rules()
    {
        return [
            [['name', 'type', 'responsible', 'cost', 'status', 'startDate', 'finishDate'], 'required'],
            [['type', 'responsible', 'department', 'cost', 'subcontractor', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['startDate', 'finishDate', 'actualfinishDate'], 'safe'],
            [['name'], 'string', 'max' => 255],
			[['name', 'department'], 'unique', 'targetAttribute' => ['name', 'department'] , 'message' => 'Already Exists!']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'responsible' => 'Responsible',
            'department' => 'Department',
            'cost' => 'Cost',
            'subcontractor' => 'Subcontractor',
            'status' => 'Status',
            'startDate' => 'Start Date',
            'finishDate' => 'Finish Date',
            'actualfinishDate' => 'Actualfinish Date (Auto)',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
	
	public static function getProjects()
	{
		$allProjects = self::find()->all();
		$allProjectsArray = ArrayHelper::
					map($allProjects, 'id', 'name');
		return $allProjectsArray;						
	}
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	public function getDepartmentItem()
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
	
	public function getSubcontractorItem()
    {
        return $this->hasOne(Subcontractor::className(), ['id' => 'subcontractor']);
    }
	
	public function getResponsibleItem()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible']);
    }
	
	public function getTypeItem()
    {
        return $this->hasOne(Type::className(), ['id' => 'type']);
    }
	
	//הצגת כל המשתמשים הקשורים לפרויקט
	public function getUsersItem(){
		return $this->hasMany(Projectpartner::className(), ['projectId' => 'id']);
		
	}
	
	public function getTaskItem(){
		return $this->hasOne(Task::className(), ['project' => 'id']);
	}
	
	//הצגת כל המשימות הקשורות לפרויקט
	public function getTasksItem(){
		return $this->hasMany(Task::className(), ['project' => 'id']);
		
	}
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
	
	//הגדרת תאריך ויוזר אוטמטית לאחר יצירה או עדכון
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public static function getProjectsWithAllStatuses()
	{
		$allProjects = self::getProjects();
		$allProjects[null] = 'All';
		$allProjects = array_reverse ( $allProjects, true );
		return $allProjects;	
	}
	
	//הופעת הפרויקט הרלוונטי בהגדרת המשתמש
	public static function getProjectss()
	{
		if(isset($_GET['id'])){
				$id = $_GET['id'];
				//$id = yii::app()->request->getQuery('id');
				//$id = Yii::$app->getRequest()->getQueryParam('id');
			    return self::findOne($id)->name;
				//return $this->hasOne(self::className(), ['id' => $id]);
			        
			   /* if($checkProject->id == $id)
			        return $checkProject->id;*/
		}
		
		/*$allProjects = self::findOne($_GET['id']);
		if($checkUser->categoryld == $_GET['id'])
		$allProjectsArray = ArrayHelper::
					map($allProjects, 'id', 'name');
		return $allProjectsArray;					
		//return $allProjects;*/
	}
	
	public function beforeSave($insert){	
		//הוספת מניעת האפשרות שמנהל פרויקט יוכל לשנות אחראי	
		$return = parent::beforeSave($insert);
		
		//ולידציה של התאריכים
		if($this->getAttribute('startDate') > $this->getAttribute('finishDate')){
			throw new UnauthorizedHttpException ('Hey, start date can not be after finish date.');
		}
		
		//אין אפשרות להגדיר תאריך התחלה לפני התאריך של היום
		if($this->getAttribute('startDate') < date('Y-m-d')){
			throw new UnauthorizedHttpException ('Hey, you can not set a start date before todays date.');
		}
		
		//כאשר הפרויקט מבוצע יוגדר אוטומטית תאריך סיום
		if($this->getAttribute('status') == 1){
			$this->actualfinishDate = date('Y-m-d');
		}
		else{
			$this->actualfinishDate = 0000-00-00;
		}
		
		//רק מי שמוגדר כמנהל פרויקט יכול להיות אחראי
		$role = Yii::$app->authManager->getRolesByUser($this->responsibleItem->id);
		if($this->getAttribute('responsible')){
			//if(!\Yii::$app->user->can('responsible'))
			if( implode(', ', array_keys($role)) != 'Project Manager')
				throw new UnauthorizedHttpException ('Hey, the user is not defined as a project manager');
		}
		
		//בדיקה שכל המשימות בוצעות
		$selection = Task::find()->all();
		if($this->isAttributeChanged('status')){
			if($this->getAttribute('status') == 1){
				foreach($selection as $id){
					if($this->getAttribute('id') == $id->project){
						if(($id->status != 2) && ($id->status != 1)){
							throw new UnauthorizedHttpException ('Hey, you can not close the project, there are open tasks.');
						}
					}
				}
			}
		}
		
		//התאמת המחלקה למשתמש האחראי
		$selectionUsers = User::find()->all();
		if (\Yii::$app->user->can('createProject')){
			foreach($selectionUsers as $id){
				if($this->getAttribute('responsible') == $id->id){
					$this->department = $id->department;
				}
			}
		}
		
		return $return;	
	}
	
	/*public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);
		//שיוך פרויקט למחלקה מתאימה לפי בחירת אחראי
		//responsible
		//department
		$selection = User::find()->all();
		if (\Yii::$app->user->can('createProject')){
			foreach($selection as $id){
			if($this->getAttribute('responsible') == $id->id){
				$this->department = $id->department;
			}
			}
		}
		
		return $return;
	}*/
}