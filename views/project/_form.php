<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Department;
use app\models\Type;
use app\models\User;
use app\models\Status;
use app\models\Subcontractor;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(Type::getTypes()) ?>
	<?php if(\Yii::$app->user->can('createProject')){ ?>
		<?= $form->field($model, 'responsible')->dropDownList(User::getUsers()) ?>
	<?php } ?>
	
    <?php //$form->field($model, 'department')->dropDownList(Department::getDepartments()) ?>

    <?= $form->field($model, 'cost')->textInput() ?>

    <?= $form->field($model, 'subcontractor')->dropDownList(Subcontractor::getSubcontractors()) ?>

    <?= $form->field($model, 'status')->dropDownList(Status::getStatuses()) ?>

    <?= $form->field($model, 'startDate')->widget(DatePicker::className(),[
								'name' => 'startDate', 
								'value' => date('Y-M-D'),
								'options' => [
									'value' => date('Y-m-d'),
								],
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
									'clearField' => true,
									'todayHighlight' => true,
								]
							]) ?>

	<?php	/*echo '<label>Start Date</label>';
			echo DatePicker::widget([
								'name' => 'startDate', 
								//'value' => date('yyyy-mm-dd'),
								//'options' => ['placeholder' => date('Y-M-D')],
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true,
								]
							]);*/ ?>
							
	<?= $form->field($model, 'finishDate')->widget(DatePicker::className(),[
								'name' => 'finishDate', 
								'value' => date('Y-M-D'),
								'options' => [
									'value' => date('Y-m-d'),
								],
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true,
									'clearField' => true,
								],
							]) ?>

    <?php /*$form->field($model, 'actualfinishDate')->widget(DatePicker::className(),[
								'name' => 'actualfinishDate', 
								'value' => date('Y-M-D'),
								'options' => [
									'value' => date('Y-m-d'),
								],
								'pluginOptions' => [
									'autoclose' => true,
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true,
									'clearField' => true,
								],
							]) */?>

    <?php //$form->field($model, 'created_at')->textInput() ?>

    <?php //$form->field($model, 'created_by')->textInput() ?>

    <?php //$form->field($model, 'updated_at')->textInput() ?>

    <?php //$form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>