<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Project;
/* @var $this yii\web\View */
/* @var $model app\models\ProjectPartner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-partner-form">

    <?php $form = ActiveForm::begin(); ?>
	<?php
		$idProject[0] = Project::getProjectss();
	?>

    <?= $form->field($model, 'userId')->dropDownList(User::getUsers()) ?>
	<?= $form->field($model, 'projectId')->dropDownList(Project::getProjects()) ?>
    <?php //$form->field($model, 'projectId')->dropDownList(['maxlength' => true, 'projectId' => $idProject[0]]) //->getInputId();   //input($idProject[0])//Html::getInputId($model, 'projectId')) //textInput(Project::getProjectss()) ?>
	<?php /*$form->field($model, 'projectId', array(
			//'type'  => 'text',
			'value' => 'projectId'))->textInput(Project::getProjectss()) */?>
	<?php /* $form->hiddenField($model, 'projectId', array(
        'type'  => 'text',
        'value' => 'projectId',
		'options' => [
			'value' => Project::getProjectss(),
			])) */?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>