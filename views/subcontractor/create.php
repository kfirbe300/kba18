<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Subcontractor */

$this->title = 'Create Subcontractor';
$this->params['breadcrumbs'][] = ['label' => 'Subcontractors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcontractor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
