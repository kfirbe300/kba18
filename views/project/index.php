<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createProject')){ ?>
    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } ?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['id']]; },],

            //'id',
            'name',
            //'type',
			[
				'attribute' => 'type',
				'label' => 'Type',
				'value' => function($model){
					return $model->typeItem->type;
				},
				'filter'=>Html::dropDownList('ProjectSearch[type]', $type, $types, ['class'=>'form-control']),
			],
			[
				'attribute' => 'responsible',
				'label' => 'Responsible',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->responsibleItem->fullname, 
					['user/view', 'id' => $model->responsibleItem->id]);
				},
				'filter'=>Html::dropDownList('ProjectSearch[responsible]', $responsible, $responsibles, ['class'=>'form-control']),
			],
			[
				'attribute' => 'department',
				'label' => 'Department',
				/*'value' => function($model){
					return $model->departmentItem->name;
				},*/
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->departmentItem->name, 
					['department/view', 'id' => $model->departmentItem->id]);
				},
				'filter'=>Html::dropDownList('ProjectSearch[department]', $department, $departments, ['class'=>'form-control']),
			],
            // 'cost',
            // 'subcontractor',
			[
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('ProjectSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],
            //'startDate',
			[
				'attribute' => 'startDate',
				'label' => 'Start Date',
				'value' => 'startDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
								'model' => $searchModel,
								'attribute' => 'startDate', 
								'value' => date('Y-M-D', strtotime('+2 days')),
								'options' => ['placeholder' => 'Select issue date ...'],
								'pluginOptions' => [
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true
								]
							]),
			],
            //'finishDate',
			[
				'attribute' => 'finishDate',
				'label' => 'Finish Date',
				'value' => 'finishDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
								'model' => $searchModel,
								'attribute' => 'finishDate', 
								'value' => date('Y-M-D', strtotime('+2 days')),
								'options' => ['placeholder' => 'Select issue date ...'],
								'pluginOptions' => [
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true
								]
							]),
			],
            // 'actualfinishDate',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('createProject')){ ?>
	<p><?= Html::a('Delete', ['deletemultiple', 'userId' => $model->userId, 'projectId' => $model->projectId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item? *If you are not responsible for the item it will not be deleted',
                'method' => 'post',
            ],
        ]) ?> </p>
	<?php } ?>
</div>