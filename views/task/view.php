<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            //'project',
			[ 				
				'label' => $model->attributeLabels()['project'],
				'format' => 'html', 
				'value' => Html::a($model->projectItem->name, 
					['project/view', 'id' => $model->projectItem->id]),	
			],
            'description:ntext',
            //'status',
			[ 
				'label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->name,	
			],
            'startDate',
            'finishDate',
            //'actualfinishDate',
			[
				'label' => $model->attributeLabels()['actualfinishDate'],
				'value' => function($model){
					if(($model->status == 1)){
						return $model->actualfinishDate;
					}
					else {
						return 'Not yet finished';
					}
				},
			],
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
			[ 
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],
			[ 
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],
            [ 
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],	
            [ 
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
        ],
    ]) ?>
	
	<?php
	//הצגת כל המשתמשים הקשורים לפרויקט
	if($model->usersItem != null){
		echo '<label style="font-size: 150%;"> Partners: </label><br>';
		echo "|";
		foreach($model->usersItem as $i){
			echo ' <a href="?r=user/view&id='.$i->userId.'" style="font-size: 150%;">'.$i->userItem->fullname.'</a> |'; 
		}
		echo "<br><br>";
	}
	else{
		echo "<br><br>";
	}
	?>
	
	<div class="form-group">
       <p><a class="btn btn-default" href="http://kfirbe.myweb.jce.ac.il/project30049/basic/web/index.php?r=taskpartner/create">Add users &raquo;</a></p>
    </div>
</div>