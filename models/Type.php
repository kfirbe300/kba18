<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "type".
 *
 * @property int $id
 * @property string $type
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
        ];
    }
    public static function getTypes()
	{
		$allTypes = self::find()->all();
		$allTypesArray = ArrayHelper::
					map($allTypes, 'id', 'type');
		return $allTypesArray;						
	}
	
	public static function getTypesWithAllStatuses()
	{
		$allTypes = self::getTypes();
		$allTypes[null] = 'All';
		$allTypes = array_reverse ( $allTypes, true );
		return $allTypes;	
	}
}
