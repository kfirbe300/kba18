<?php

namespace app\controllers;

use Yii;
use app\models\Taskpartner;
use app\models\TaskpartnerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Task;
use app\models\User;
use yii\web\UnauthorizedHttpException;

class TaskpartnerController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['index', 'create'],
				'rules' => [
					[
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['createTask'],
					],
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['readOnly'],
					],	
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
					'deletemultiple' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TaskPartnerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 8];
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'tasks' => Task::getTasksWithAllStatuses(),
			'task' => $searchModel->taskId,
			'users' => User::getResponsiblesWithAllStatuses(),
			'user' => $searchModel->userId,
        ]);
    }

    public function actionView($userId, $taskId)
    {
        return $this->render('view', [
            'model' => $this->findModel($userId, $taskId),
        ]);
    }

    public function actionCreate()
    {
        $model = new TaskPartner();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'userId' => $model->userId, 'taskId' => $model->taskId]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($userId, $taskId)
    {
        $model = $this->findModel($userId, $taskId);
		
		//בדיקת הרשאה
		if (!\Yii::$app->user->can('updateTaskpartner', ['taskPartner' =>$model]) )
			throw new UnauthorizedHttpException('Hey, you are not allowed to update a partner for task that you not responsible for');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'userId' => $model->userId, 'taskId' => $model->taskId]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($userId, $taskId)
    {
		$model = $this->findModel($userId, $taskId);
		
		//בדיקת הרשאה
		if (!\Yii::$app->user->can('deleteTaskpartner', ['taskPartner' =>$model]) )
			throw new UnauthorizedHttpException('Hey, you are not allowed to delete a partner for task that you not responsible for');
		
        $this->findModel($userId, $taskId)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($userId, $taskId)
    {
        if (($model = TaskPartner::findOne(['userId' => $userId, 'taskId' => $taskId])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionDeletemultiple(){
       $action = Yii::$app->request->post('action');
       $selection = (array)Yii::$app->request->post('selection');
	   $userLogin = Yii::$app->user->identity->id;
       //$checkUser = $userLogin->userItem->id;
	   if(!\Yii::$app->user->can('deleteUser')){
		   foreach($selection as $id){
			   $e = Taskpartner::findOne(['userId' => $id]);
			   $checkTask = Task::findOne(['id' => $e]);
			   if(\Yii::$app->user->can('deleteTaskpartner')){
					$e->delete();
			   }
			   else{
				   $res = $checkTask->projectItem->responsible;
				   if($userLogin == $res){
					   $e->delete();
				   }
			   }
		  }
	  }
	  else{
		  foreach($selection as $id){
			  $e = Taskpartner::findOne(['userId'=> $id]);
			  $e->delete();
		  }
	  }
		
		return $this->redirect(['index']);
	}
}