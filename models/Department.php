<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\User;
/**
 * This is the model class for table "department".
 *
 * @property int $id
 * @property string $name
 * @property string $headDepartment
 * @property int $budget
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'headDepartment', 'budget'], 'required'],
            [['budget'], 'integer'],
            [['name', 'headDepartment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'headDepartment' => 'Head Department',
            'budget' => 'Budget',
        ];
    }

    public static function getDepartments()
	{
		$allDepartments = self::find()->all();
		$allDepartmentsArray = ArrayHelper::
                    map($allDepartments, 'id', 'name');
                    $allDepartmentsArray[null] = 'Null';
		return $allDepartmentsArray;						
	}
	
	public static function getDepartmentsWithAllStatuses()
	{
		$allDepartments = self::getDepartments();
		$allDepartments[null] = 'All';
		$allDepartments = array_reverse ( $allDepartments, true );
		return $allDepartments;	
    }
    
    public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'headDepartment']);
    }
    public function beforeSave($insert){	
		$return = parent::beforeSave($insert);
		
		//מי שמוגדר כמבצע משימה לא יוכל להיות ראש מחלקה
		$role = Yii::$app->authManager->getRolesByUser($this->headDepartment);
		if($this->getAttribute('headDepartment')){
			if( (implode(', ', array_keys($role)) != 'Project Manager') && (implode(', ', array_keys($role)) != 'Admin'))
				throw new UnauthorizedHttpException ('Hey, the user is can not defined as a head of department');
		}
		
		return $return;	
	}
}
