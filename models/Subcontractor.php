<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "subcontractor".
 *
 * @property int $id
 * @property string $name
 * @property string $contact
 * @property int $phone
 */
class Subcontractor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subcontractor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['phone'], 'integer'],
            [['name', 'contact'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'contact' => 'Contact',
            'phone' => 'Phone',
        ];
    }

    public static function getSubcontractors()
	{
		$allSubcontractors = self::find()->all();
		$allSubcontractorsArray = ArrayHelper::
					map($allSubcontractors, 'id', 'name');
		$allSubcontractorsArray[null] = 'Null';
		return $allSubcontractorsArray;						
	}
}
