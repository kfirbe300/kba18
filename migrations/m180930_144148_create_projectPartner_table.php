<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projectPartner`.
 */
class m180930_144148_create_projectPartner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projectPartner', [
            'userId' => $this->integer()->notNull(),
			'projectId' => $this->integer()->notNull(),
        ]);
		
		$this->addPrimaryKey('projectPartner_pk', 'projectPartner', ['userId', 'projectId']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('projectPartner');
    }
}
