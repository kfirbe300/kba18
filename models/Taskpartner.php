<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\Task;
use app\models\Projectpartner;
use app\models\Project;
use yii\web\UnauthorizedHttpException;
/**
 * This is the model class for table "taskPartner".
 *
 * @property integer $userId
 * @property integer $taskId
 */
class Taskpartner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taskPartner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'taskId'], 'required'],
            [['userId', 'taskId'], 'integer'],
			[['userId', 'taskId'], 'unique', 'targetAttribute' => ['userId', 'taskId'] , 'message' => 'Already Exists!']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'User',
            'taskId' => 'Task',
        ];
    }
	
	public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
	
	public function getTaskItem()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskId']);
    }
	
	public function getProjectpartnerItem()
    {
        return $this->hasOne(Projectpartner::className(), ['userId' => 'userId']);
    }
	
	public function getProjectItem()
    {
        return $this->hasOne(Project::className(), ['responsible' => 'userId']);
    }
	
	//שיוך משתמש שלא קשור למשימה
	public function beforeSave($insert) 
    {		
		$return = parent::beforeSave($insert);
		$flag = false;
		$selection = Projectpartner::find()->all();
		//foreach(self::getProjectpartnerItem() as $i){
		foreach($selection as $i){
			//if(($this->getAttribute('taskId') == $i->taskItem->id) && ($i->taskItem->project == $i->projectpartnerItem->projectId) && ($i->projectpartnerItem->userId == $this->getAttribute('userId')))
			//בדיקה האם המשתמש שותף לפרויקט
			if(($i->userId == $this->getAttribute('userId'))){
				if(($this->taskItem->project == $i->projectId)){
					$flag = true;
				}
			}
			//בדיקה האם המשתמש אחראי לפרויקט
			/*if($this->getAttribute('userId') == $i->projectItem->responsible){
				if($i->projectItem->id == $this->taskItem->project){
					$flag = true;
				}
			}*/
		}
		
		//בדיקה האם המשתמש אחראי לפרויקט
		$checkTask = Task::findOne($this->getAttribute('taskId'));
		if($this->getAttribute('userId') == $checkTask->projectItem->responsible){
			//if($this->getAttribute('taskId') == $this->taskItem->id){
				//if($this->projectItem->id == $this->taskItem->project)
					$flag = true;
			//}
		}
		
		if(!$flag){
			throw new UnauthorizedHttpException ('Hey, this user does not belong to the project.');
		}
		
		//משתמש חייב להיות מוגדר כמבצע משימה
		$role = Yii::$app->authManager->getRolesByUser($this->userItem->id);
		if($this->getAttribute('userId')){
			if(implode(', ', array_keys($role)) != 'Perform Task')
				throw new UnauthorizedHttpException ('Hey, this user can not be task partner, only a perform task.');
		}
		
		return $return;	
    }
	
	public function getFullname()
    {
        return $this->userItem->fullname.' belong to '.$this->taskItem->name;
    }
}