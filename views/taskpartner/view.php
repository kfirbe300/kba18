<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaskPartner */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Task Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-partner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'userId' => $model->userId, 'taskId' => $model->taskId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'userId' => $model->userId, 'taskId' => $model->taskId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
         //   'userId',
         [ 				
            'label' => $model->attributeLabels()['userId'],
            'format' => 'html', 
            'value' => Html::a($model->userItem->fullname, 
                ['user/view', 'id' => $model->userItem->id]),	
        ],
            'taskId',
            [ 				
				'label' => $model->attributeLabels()['taskId'],
				'format' => 'html', 
				'value' => Html::a($model->taskItem->name, 
					['task/view', 'id' => $model->taskItem->id]),	
			],
        ],
    ]) ?>

</div>
