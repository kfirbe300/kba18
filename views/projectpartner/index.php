<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectPartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Partners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-partner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<h4>Here you can see all the partners for all the projects</h4>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
    <p>
        <?= Html::a('Add Project Partner', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } ?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>

	<?php //Html::submitButton('Send', ['class' => 'btn btn-info',]);?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['userId']]; },], //'yii\grid\SerialColumn'
            //'userId',
			[
				'attribute' => 'userId',
				'label' => 'User',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->userItem->fullname, 
					['user/view', 'id' => $model->userItem->id]);
				},
				'filter'=>Html::dropDownList('ProjectpartnerSearch[userId]', $user, $users, ['class'=>'form-control']),
			],
            //'projectId',
			[
				'attribute' => 'projectId',
				'label' => 'Project',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->projectItem->name, 
					['project/view', 'id' => $model->projectItem->id]);
				},
				'filter'=>Html::dropDownList('ProjectpartnerSearch[projectId]', $project, $projects, ['class'=>'form-control']),
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
	<p><?= Html::a('Delete', ['deletemultiple', 'userId' => $model->userId, 'projectId' => $model->projectId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item? *If you are not responsible for the item it will not be deleted',
                'method' => 'post',
            ],
        ]) ?> </p>
	<?php } ?>
</div>