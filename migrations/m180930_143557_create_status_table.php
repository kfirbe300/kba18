<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m180930_143557_create_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('status', [
                       'id' => $this->primaryKey(),
            			'name' => 'string',
                    ],
            		'ENGINE=InnoDB'
            		);
               }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('status');
    }
}
