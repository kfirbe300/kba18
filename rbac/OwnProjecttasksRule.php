<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
use app\models\User;
use app\models\Project;
use app\models\Task;

class OwnProjecttasksRule extends Rule
{
	public $name = 'ownProjecttasksRule';
	
	/*public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			//return isset($params['post']) ? $params['post']->id->responsibleItem() == $user : false;
			return isset($params['post']) ? $params['post']->responsible == $user : false;
		}
		return false;
	}*/
	
	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			if(isset($params['task'])){
				$checkUser = Task::findOne($params['task'])->projectItem->responsible;
				
			    $checkProject = Project::findOne($user);
			        
			    if($checkUser == $user)
			        return true;
			}
		}
		return false;
	}
}