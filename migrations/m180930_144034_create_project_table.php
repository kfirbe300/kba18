<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m180930_144034_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'type' => $this->integer()->notNull(),
			'responsible' => $this->integer()->notNull(),
			'department' => $this->integer()->notNull(),
			'cost' => $this->integer()->notNull(),
			'subcontractor' => $this->integer(),
			'status' => $this->integer()->notNull(),
			'startDate' => $this->DATE()->notNull(),
			'finishDate' => $this->DATE()->notNull(),
			'actualfinishDate' => $this->DATE()->notNull(),
			'created_at' => $this->integer(),
			'created_by' => $this->integer(),
			'updated_at' => $this->integer(),
			'updated_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('project');
    }
}
