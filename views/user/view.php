<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'username',
            'firstname',
            'lastname',
            //'password',
            //'department',
			[ 
				'label' => $model->attributeLabels()['department'],
				//'value' => $model->departmentItem->name,	
				'format' => 'html',
				'value' => function($model){
					if($model->departmentItem){
						return Html::a($model->departmentItem->name, 
							['department/view', 'id' => $model->departmentItem->id]);
					}
					else {
						return 'General';
					}
				},
			],
            //'authKey',
			//'role',
			[ 
				'label' => $model->attributeLabels()['role'],
				  'value' => function($model) {
					$roles = Yii::$app->authManager->getRolesByUser($model->id);
					if ($roles) {
						return implode(', ', array_keys($roles));
					} 
					else {
						return 'No Role';
					}
				},
			],
        ],
    ]) ?>
	
	<?php
	if($model->responsibleItem != null){
		echo '<div class="col-lg-3">';
		echo '<label style="font-size: 150%;"><u> Responsible For: </u></label><br>';
		echo "|";
		foreach($model->responsibleItem as $i){
			echo ' <a href="?r=project/view&id='.$i->id.'" style="font-size: 150%;">'.$i->name.'</a> |'; 
		}
		echo "<br><br>";
		echo "</div>";
	}
	?>
	
	<?php
	if($model->projectsItem != null){
		echo '<div class="col-lg-3">';
		echo '<label style="font-size: 150%;"><u> Projects: </u></label><br>';
		echo "|";
		foreach($model->projectsItem as $i){
			echo ' <a href="?r=project/view&id='.$i->projectItem->id.'" style="font-size: 150%;">'.$i->projectItem->name.'</a> |'; 
		}
		echo "<br><br>";
		echo "</div>";
	}
	?>
	
	<?php
	if($model->tasksItem != null){
		echo '<div class="col-lg-3">';
		echo '<label style="font-size: 150%;"><u> Tasks: </u></label><br>';
		echo "|";
		foreach($model->tasksItem as $i){
			echo ' <a href="?r=task/view&id='.$i->taskItem->id.'" style="font-size: 150%;">'.$i->taskItem->name.'</a> |'; 
		}
		echo "<br><br>";
		echo "</div>";
	}

	?>

</div>