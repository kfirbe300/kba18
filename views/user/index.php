<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createUser')){ ?>
    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } ?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['id']]; },],

            //'id',
            'username',
            'firstname',
            'lastname',
            //'password',
            //'department',
			[
				'attribute' => 'department',
				'label' => 'Department',
				/*'value' => function($model){
					return $model->departmentItem->name;
				},*/
				'format' => 'html',
				'value' => function($model){
					if($model->departmentItem){
						return Html::a($model->departmentItem->name, 
							['department/view', 'id' => $model->departmentItem->id]);
					}
					else {
						return 'General';
					}
				},
				'filter'=>Html::dropDownList('UserSearch[department]', $department, $departments, ['class'=>'form-control']),
			],
            // 'authKey',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('deleteUser')){ ?>
	<p><?= Html::a('Delete', ['deletemultiple', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item? *If you are not responsible for the item it will not be deleted',
                'method' => 'post',
            ],
        ]) ?> </p>
	<?php } ?>
</div>