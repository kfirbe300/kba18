<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
    <p>
        <?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } ?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['id']]; },],

            //'id',
            'name',
            //'project',
			[
				'attribute' => 'project',
				'label' => 'Project',
				'format' => 'html',
				'value' => function($model){
					return Html::a($model->projectItem->name, 
					['project/view', 'id' => $model->projectItem->id]);
				},
				'filter'=>Html::dropDownList('TaskSearch[project]', $project, $projects, ['class'=>'form-control']),
			],
            //'description:ntext',
            //'status',
			[
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('TaskSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],
            //'startDate',
            //'finishDate',
			[
				'attribute' => 'startDate',
				'label' => 'Start Date',
				'value' => 'startDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
								'model' => $searchModel,
								'attribute' => 'startDate', 
								'value' => date('Y-M-D', strtotime('+2 days')),
								'options' => ['placeholder' => 'Select issue date ...'],
								'pluginOptions' => [
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true
								]
							]),
			],
			[
				'attribute' => 'finishDate',
				'label' => 'Finish Date',
				'value' => 'finishDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
								'model' => $searchModel,
								'attribute' => 'finishDate', 
								'value' => date('Y-M-D', strtotime('+2 days')),
								'options' => ['placeholder' => 'Select issue date ...'],
								'pluginOptions' => [
									'format' => 'yyyy-mm-dd',
									'todayHighlight' => true
								]
							]),
			],
            // 'actualfinishDate',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('createTask')){ ?>
	<p><?= Html::a('Delete', ['deletemultiple', 'userId' => $model->userId, 'projectId' => $model->projectId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?*If you are not responsible for the item it will not be deleted',
                'method' => 'post',
            ],
        ]) ?> </p>
	<?php } ?>
</div>