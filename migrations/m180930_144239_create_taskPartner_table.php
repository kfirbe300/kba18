<?php

use yii\db\Migration;

/**
 * Handles the creation of table `taskPartner`.
 */
class m180930_144239_create_taskPartner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('taskPartner', [
            'userId' => $this->integer()->notNull(),
			'taskId' => $this->integer()->notNull(),
        ]);
		
		$this->addPrimaryKey('taskPartner_pk', 'taskPartner', ['userId', 'taskId']);
    }
    

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('taskPartner');
    }
}
