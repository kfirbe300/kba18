<?php

/* @var $this yii\web\View */

$this->title = 'S.S projects';
?>
<div class="site-index" style="color: #527a7a;">

    <div class="jumbotron" style="margin:0 0 0 auto;">
        <h1 style="font-weight: bold; font-size: 700%; margin:0 0 0 auto;">S.S Projects</h1>

        <p class="lead" style="font-size: 300%; margin:0 0 0 auto;">Project Management System</p>
		<?php if(Yii::$app->user->isGuest){ ?>
        <p><a class="btn btn-lg btn-success" href="http://kfirbe.myweb.jce.ac.il/project30049/basic/web/index.php?r=site%2Flogin">Click To Login</a></p>
		<?php } ?>
    </div>
	<?php
	if(!Yii::$app->user->isGuest){ ?>
    <div align="center" class="body-content" style="margin:0 0 0 auto;">

        <div class="row" style="margin:0 0 0 auto;">
            <div class="col-lg-3" style="margin-left:15%;"> <!--style="margin-left:15%;"-->
                <h2 style="font-weight: bold; font-size: 300%;">Projects</h2>

                <!--<p style="font-size: 125%;">Add, update and delete projects.</p>-->

                <p><a class="btn btn-default" href="http://kfirbe.myweb.jce.ac.il/project30049/basic/web/index.php?r=project">Index &raquo;</a></p>
				<?php if(Yii::$app->user->can('createTask')){ ?>
				<p><a class="btn btn-default" href="http://kfirbe.myweb.jce.ac.il/project30049/basic/web/index.php?r=projectpartner/create">Add User &raquo;</a></p>
				<?php } ?>
			</div>
            <div class="col-lg-3" style="margin-left:20%;"> <!--style="margin-left:20%;"-->
                <h2 style="font-weight: bold; font-size: 300%;">Tasks</h2>

                <!--<p style="font-size: 125%;">Add, update and delete tasks.</p>-->

                <p><a class="btn btn-default" href="http://kfirbe.myweb.jce.ac.il/project30049/basic/web/index.php?r=task">Index   &raquo;</a></p>
				<?php if(Yii::$app->user->can('createTask')){ ?>
				<p><a class="btn btn-default" href="http://kfirbe.myweb.jce.ac.il/project30049/basic/web/index.php?r=taskpartner/create">Add User &raquo;</a></p>
				<?php } ?>
			</div>
			<!--<div class="col-lg-3" style="margin-left:5%;">
                <h2 style="font-weight: bold; font-size: 300%;">Actions:</h2>

                <p style="font-size: 125%;">Add, update and delete tasks.</p>

                <p><a class="btn btn-default" href="http://kfirbe.myweb.jce.ac.il/project30049/basic/web/index.php?r=task">Click &raquo;</a></p>
            </div>-->
        </div>

    </div>
	<?php } ?>
</div>