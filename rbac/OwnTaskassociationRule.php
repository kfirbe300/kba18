<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
use app\models\User;
use app\models\Project;
use app\models\Task;
use app\models\Taskpartner;

class OwnTaskassociationRule extends Rule
{
	public $name = 'ownTaskassociationRule';
	
	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			//if(isset($params['taskPartner'])){
			if(isset($_GET['taskId'])){	
				/*//$checkUser = Taskpartner::findOne($user)->projectItem->responsible;
				//$checkProject = Taskpartner::findOne($params['taskPartner'])->projectItem->id;
				$checkTaskRes = Task::findOne($params['taskpartner'])->projectItem->responsible;
				$checkUser = $params['taskPartner']->projectItem->responsible;
				
				if($user == $checkUser)
					return true;*/
				
				$checkUser = User::findOne($user);
			    $checkTask = Task::findOne($_GET['taskId'])->projectItem->responsible;
			    if($checkUser->id == $checkTask)
			        return true;
			}
			/*else if(isset($params['taskPartner'])){
				$checkTask = Task::findOne($_GET['taskId'])->projectItem->responsible;
				$checkUser = User::findOne($user);
				if($checkUser == $checkTask)
			        return true;
				
			}*/
			//return isset($params['taskpartner']) ? $params['taskpartner']->responsible == $user : false;
		}
		return false;
	}
}