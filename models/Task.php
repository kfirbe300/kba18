<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Project;
use app\models\Status;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;
use app\models\Taskpartner;

class Task extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'task';
    }

    public function rules()
    {
        return [
            [['name', 'project', 'description', 'status', 'startDate', 'finishDate'], 'required'],
            [['project', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['startDate', 'finishDate', 'actualfinishDate'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name', 'project'], 'unique', 'targetAttribute' => ['name', 'project'] , 'message' => 'Already Exists!']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'project' => 'Project',
            'description' => 'Description',
            'status' => 'Status',
            'startDate' => 'Start Date',
            'finishDate' => 'Finish Date',
            'actualfinishDate' => 'Actualfinish Date (Auto)',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
	
	public function getProjectItem()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
	
	//הגדרת תאריך ויוזר אוטמטית לאחר יצירה או עדכון
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public static function getTasks()
	{
		$allTasks = self::find()->all();
		$allTasksArray = ArrayHelper::
					map($allTasks, 'id', 'name');
		return $allTasksArray;						
	}
	
	public static function getTasksWithAllStatuses()
	{
		$allTasks = self::getTasks();
		$allTasks[null] = 'All';
		$allTasks = array_reverse ( $allTasks, true );
		return $allTasks;	
	}
	
	//הצגת כל המשתמשים הקשורים למשימה
	public function getUsersItem(){
		return $this->hasMany(Taskpartner::className(), ['taskId' => 'id']);
		
	}
	
	public function beforeSave($insert){		
		$return = parent::beforeSave($insert);
		
		//ולידציה של התאריכים
		if($this->getAttribute('startDate') > $this->getAttribute('finishDate')){
			throw new UnauthorizedHttpException ('Hey, start date can not be after finish date.');
		}
		
		//אין אפשרות להגדיר תאריך התחלה לפני התאריך של היום
		if($this->getAttribute('startDate') < date('Y-m-d')){
			throw new UnauthorizedHttpException ('Hey, you can not set a start date before todays date.');
		}
		
		//כאשר הפרויקט מבוצע יוגדר אוטומטית תאריך סיום
		if($this->getAttribute('status') == 1){
			$this->actualfinishDate = date('Y-m-d');
		}
		else{
			$this->actualfinishDate = 0000-00-00;
		}
		
		//מניעת האפשרות לפתוח משימה לפרויקט שנגמר
		if(($this->projectItem->status == 1) || ($this->projectItem->status == 2)){
			throw new UnauthorizedHttpException ('Hey, project closed you can not add more tasks.');
        }
        //אחראי פרויקט יכול להגדיר רק לפרויקט שלו
		/*$checkUser = Yii::$app->user->identity->id;
		if (!\Yii::$app->user->can('createProject')){
			if($this->isAttributeChanged('project')){
				if($this->getAttribute('project') == $this->projectItem->id){
					if($checkUser != $this->projectItem->responsible)
						throw new UnauthorizedHttpException ('Hey, you not responsible for this project.');
				}
			}
		}*/
		
		if (!\Yii::$app->user->can('createProject')){
			$checkTasks = Taskpartner::find()->all();
			$user = Yii::$app->user->identity->id;
			$checkUserRes = $this->projectItem->responsible;
			//מניעת עדכון סטטוס על-ידי מי שלא מוגדר כמבצע משימה
			foreach($checkTasks as $i){
				if($user == $i->userId){
					if($this->getAttribute('id') != $i->taskId)
						unset($this->status);
				}
			}
			//בדיקה האם המשתמש אחראי או לא, מונע עדכון
			if(\Yii::$app->user->can('createTask')){
				if($this->getAttribute('project') == $this->projectItem->id){
					if($user != $this->projectItem->responsible)
						throw new UnauthorizedHttpException ('Hey, you not responsible for this project.');
				}
			}
			
		}
		
		
		return $return;	
	}
}