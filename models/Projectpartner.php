<?php

namespace app\models;

use Yii;
use app\models\Project;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\web\UnauthorizedHttpException;

class Projectpartner extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'projectPartner';
    }

    public function rules()
    {
        return [
            [['userId', 'projectId'], 'required'],
            [['userId', 'projectId'], 'integer'],
			[['userId', 'projectId'], 'unique', 'targetAttribute' => ['userId', 'projectId'] , 'message' => 'Already Exists!']
        ];
    }

    public function attributeLabels()
    {
        return [
            'userId' => 'User',
            'projectId' => 'Project',
        ];
    }
	
	public function getProjectItem()
    {
        return $this->hasOne(Project::className(), ['id' => 'projectId']);
    }
	
	public function getUserItem()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
	
	public static function getProjectpartners()
	{
		$allDProjectpartners = self::find()->all();
		$allProjectpartnersArray = ArrayHelper::
					map($allProjectpartners, 'id', 'name');
		return $allProjectpartnersArray;						
	}
	
	public function beforeSave($insert){		
		$return = parent::beforeSave($insert);
		
		//אם המשתמש מוגדר כאחראי לא נגדיר אותו שוב כמבצע
		if($this->isAttributeChanged('userId')){
			if($this->getAttribute('userId') == $this->projectItem->responsible){
				throw new UnauthorizedHttpException ('Hey, the user is responsible for the project there is no need to be defined as a partner.');
			}
		}
		
		//אחראי פרויקט יכול להגדיר רק לפרויקט שלו
		$checkUser = Yii::$app->user->identity->id;
		if($this->isAttributeChanged('projectId')){
			if (!\Yii::$app->user->can('createProjectpartner')){
				if($this->getAttribute('projectId') == $this->projectItem->id){
					if($checkUser != $this->projectItem->responsible)
						throw new UnauthorizedHttpException ('Hey, you not responsible for this project.');
				}
			}
		}
		
		//אם לא מוגדר למשתמש תפקיד, לא ניתן להגדיר כמבצע פרויקט
		$role = Yii::$app->authManager->getRolesByUser($this->userItem->id);
		if($this->getAttribute('userId')){
			if((implode(', ', array_keys($role)) != 'Project Manager') && (implode(', ', array_keys($role)) != 'Perform Task') && (implode(', ', array_keys($role)) != 'Admin'))
				throw new UnauthorizedHttpException ('Hey, this user can not be project partner.');
		}
		
		//$id = yii::app()->request->getQuery('id');
		/*$id = $_GET['projectId'];
		if(isset($_GET['projectId'])){
				$this->$id;
		}*/

		/*if($this->isAttributeChanged('status')){
			if (!\Yii::$app->user->can('changeToPublish')){
				if($this->getAttribute('status') == 2)
					unset($this->status);
			}
		}*/
		
		return $return;	
	}
	
	public function getFullname()
    {
        return $this->userItem->fullname.' belong to '.$this->projectItem->name;
    }
}