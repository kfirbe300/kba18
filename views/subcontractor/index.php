<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubcontractorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subcontractors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcontractor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?php if(\Yii::$app->user->can('createUser')){ ?>
    <p>
        <?= Html::a('Create Subcontractor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php } ?>
	<?=Html::beginForm(['controller/bulk'],'post');?>

	<?=Html::a('','action')?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn',
			'checkboxOptions' => function($model, $key, $index, $widget) {
				return ['value' => $model['id']]; },],

            //'id',
            'name',
            'contact',
            'phone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php if(\Yii::$app->user->can('deleteUser')){ ?>
	<p><?= Html::a('Delete', ['deletemultiple', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item? *If you are not responsible for the item it will not be deleted',
                'method' => 'post',
            ],
        ]) ?> </p>
	<?php } ?>
</div>