<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
use app\models\User;
use app\models\Project;
use app\models\Projectpartner;

class OwnProjectassociationRule extends Rule
{
	public $name = 'ownProjectassociationRule';
	
	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			/*if(isset($params['projectPartner'])){
				$checkUser = Projectpartner::findOne($params['projectPartner'])->projectItem->responsible;
				
			    $checkProject = Project::findOne($user);
			        
			    if($user->id == $checkUser)
			        return true;

			}*/
			//return isset($params['project']) ? $params['project']->responsible == $user : false;
			
			$checkUser = User::findOne($user);
			$checkProject = Project::findOne($_GET['projectId'])->responsible;
			if($checkUser->id == $checkProject)
			   return true;
		}
		return false;
	}
}