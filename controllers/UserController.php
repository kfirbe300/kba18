<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Department;
use app\models\Taskpartner;
use app\models\Projectpartner;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

class UserController extends Controller
{

    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['create', 'delete', 'index'],
				'rules' => [
					[
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['createUser'],
					],
					/*[
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['updateUser'],
					],*/
					[
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['deleteUser'],
					],	
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['readOnly'],
					],	
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 8];
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'departments' => Department::getDepartmentsWithAllStatuses(),
			'department' => $searchModel->department,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		if (!\Yii::$app->user->can('updateUser', ['user' =>$model]) )
			throw new UnauthorizedHttpException('Hey, You are not allowed to update a user that is not your own');
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
		//מחיקת המשתמש כשותף
		$tasks = Taskpartner::find()->all();
		foreach($tasks as $i){
			if($this->findModel($id)->id == $i->userId){
				$i->delete();
			}
		}
		
		$projects = Projectpartner::find()->all();
		foreach($projects as $j){
			if($this->findModel($id)->id == $j->userId){
				$j->delete();
			}
		}
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionDeletemultiple(){
	   if(\Yii::$app->user->can('deleteUser')){
		   $action=Yii::$app->request->post('action');
		   $selection=(array)Yii::$app->request->post('selection');//typecasting
		   foreach($selection as $id){
				$e = User::findOne(['id'=> $id]);
				//מחיקת המשתמש כשותף
				$tasks = Taskpartner::find()->all();
				foreach($tasks as $i){
					if($e->id == $i->taskId){
						$i->delete();
					}
				}
				
				$projects = Projectpartner::find()->all();
				foreach($projects as $j){
					if($e->id == $j->projectId){
						$j->delete();
					}
				}
				$e->delete();
		  }
	   }
		
		return $this->redirect(['index']);
	}
}