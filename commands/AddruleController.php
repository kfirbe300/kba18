<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{
	//התאמת משימה לאחראי על פרויקט
	public function actionOwnprojecttasks()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnProjecttasksRule;
		$auth->add($rule);
	}
	
	//התאמת אחראי לפרויקט שלו
	public function actionOwnproject()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnProjectRule;
		$auth->add($rule);
	}
	
	//התאמת הוספת שותף לפרויקט לאחראי על פרויקט
	public function actionOwnprojectassociation()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnProjectassociationRule;
		$auth->add($rule);
	}
	
	public function actionOwntaskassociation()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnTaskassociationRule;
		$auth->add($rule);
	}
	
	//התאמת יוזר למשתמש שלו
	public function actionOwnuser()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnUserRule;
		$auth->add($rule);
	}
}