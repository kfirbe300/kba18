<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Department;
use app\models\Task;
use app\models\Project;
use app\models\Projectpartner;
use app\models\Taskpartner;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
	public $role;
	
    public static function tableName(){
		return 'user';
	}
	
	public function rules(){
		return 
		[
			[['username','password','authKey','firstname','lastname'],'string','max' => 255],
			[['department'], 'integer'],
			[['username','password'],'required'],
			[['username'],'unique','message' => 'Already Exists!'],
			['role', 'safe'],
		];
	}
	
	public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'First Name',
			'lastname' => 'Last Name',
			'department' => 'Department',
			'role' => 'Role',
        ];
    }

    public static function findIdentity($id)
    {
		return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('Not supported');
		return null;
    }

    public static function findByUsername($username)
    {
		return self::findOne(['username' => $username]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	public function beforeSave($insert) 
    {
        $return = parent::beforeSave($insert);
        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);
        if ($this->isNewRecord)
		    $this->authKey = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
	public static function getRoles()
	{
		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		$roles[null] = 'Null';
		return $roles; 		
	}
	
	public function afterSave($insert,$changedAttributes)
    {
		//שמירת התפקיד בטבלה המתאימה
        $return = parent::afterSave($insert, $changedAttributes);
		
		//משתמש לא יכול לעדכן לעצמו תפקיד אלא אם מדובר במנהל מערכת
		if (\Yii::$app->user->can('assignRole')){
			//בדיקה האם למשתמש הזה אין תפקיד מוגדר
			if($this->role != null){ //getAttribute('role')
				$auth = Yii::$app->authManager;
				$roleName = $this->role; 
				$role = $auth->getRole($roleName);
				if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
					$auth->assign($role, $this->id);
				} else {
					$db = \Yii::$app->db;
					$db->createCommand()->delete('auth_assignment',
						['user_id' => $this->id])->execute();
					$auth->assign($role, $this->id);
				}
			}
			else{
				$db = \Yii::$app->db;
				$db->createCommand()->delete('auth_assignment',
					['user_id' => $this->id])->execute();
			}
		}

        return $return;
    }
	
	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }
	
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'fullname');
		return $users;						
	}
	
	public function getDepartmentItem()
    {
        return $this->hasOne(Department::className(), ['id' => 'department']);
    }
	
	public static function getResponsiblesWithAllStatuses()
	{
		$allUsers = self::getUsers();
		$allUsers[null] = 'All';
		$allUsers = array_reverse ( $allUsers, true );
		return $allUsers;	
	}
	
	public function getTasksItem(){
		return $this->hasMany(Taskpartner::className(), ['userId' => 'id']);	
	}
	
	public function getProjectsItem(){
		return $this->hasMany(Projectpartner::className(), ['userId' => 'id']);
		
	}
	
	public function getResponsibleItem(){
		return $this->hasMany(Project::className(), ['responsible' => 'id']);
		
	}
}