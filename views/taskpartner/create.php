<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaskPartner */

$this->title = 'Add Task Partner';
$this->params['breadcrumbs'][] = ['label' => 'Task Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-partner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
