<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subcontractor */

$this->title = 'Update Subcontractor: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Subcontractors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subcontractor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
