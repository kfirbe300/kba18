<?php

namespace app\controllers;

use Yii;
use app\models\Project;
use app\models\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Status;
use app\models\Department;
use app\models\User;
use app\models\Type;
use app\models\Task;
use app\models\Projectpartner;
use yii\web\UnauthorizedHttpException;

class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['create', 'index', 'delete'],
				'rules' => [
					[
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['createProject'],
					],
					/*[
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['updateProject'],
					],*/
					[
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['deleteProject'],
					],
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['readOnly'],
					],	
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
					'deletemultiple' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 4];
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'statuses' => Status::getStatusesWithAllStatuses(),
			'status' => $searchModel->status,
			'departments' => Department::getDepartmentsWithAllStatuses(),
			'department' => $searchModel->department,
			'responsibles' => User::getResponsiblesWithAllStatuses(),
			'responsible' => $searchModel->responsible,
			'types' => Type::getTypesWithAllStatuses(),
			'type' => $searchModel->type,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		//רק אחראי או מנהל מערכת יכול לעדכן את הפרויקט
		if (!\Yii::$app->user->can('updateProject', ['project' =>$model]) )
			throw new UnauthorizedHttpException('Hey, you are not allowed to update a project you not responsible for');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
		/*$model = $this->findModel($id);
		//רק אחראי יכול למחוק את הפרויקט שלו
		if (!\Yii::$app->user->can('deleteProject', ['project' =>$model]) )
			throw new UnauthorizedHttpException('Hey, you are not allowed to delete a project you not responsible for');*/
		
		//מחיקת כל השותפים הקשורים לפרויקט
		$projectsP = Projectpartner::find()->all();
		foreach($projectsP as $i){
			if($this->findModel($id)->id == $i->projectId){
				$i->delete();
			}
		}
		//מחיקת משימות הקשורות לפרויקט
		$tasks = Task::find()->all();
		foreach($tasks as $j){
			if($this->findModel($id)->id == $j->project){
				$j->delete();
			}
		}
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionDeletemultiple(){
       $action=Yii::$app->request->post('action');
       $selection=(array)Yii::$app->request->post('selection');
	   if (\Yii::$app->user->can('deleteProject')){
		   foreach($selection as $id){
				$e = Project::findOne(['id'=> $id]);
				$select = Projectpartner::find()->all();
				foreach($select as $i){
					if($e->id == $i->projectId){
						$i->delete();
					}
				}
					
				//מחיקת משימות הקשורות לפרויקט
				$tasks = Task::find()->all();
				foreach($tasks as $j){
					if($e->id == $j->project){
						$j->delete();
					}
				}
				$e->delete();
		   }
	  }
		
	return $this->redirect(['index']);
	}
}