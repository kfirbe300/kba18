<?php

namespace app\controllers;

use Yii;
use app\models\Projectpartner;
use app\models\ProjectpartnerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Project;
use app\models\User;
use yii\web\UnauthorizedHttpException;
use app\models\Taskpartner;

class ProjectpartnerController extends Controller
{

    public function behaviors()
    {
        return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['index', 'create'],
				'rules' => [
					[
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['createTask'],
					],
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['readOnly'],
					],	
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
					'deletemultiple' => ['POST'],
                ],
            ],
        ];
    }

	
    public function actionIndex()
    {
        $searchModel = new ProjectPartnerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 8];
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'projects' => Project::getProjectsWithAllStatuses(),
			'project' => $searchModel->projectId,
			'users' => User::getResponsiblesWithAllStatuses(),
			'user' => $searchModel->userId,
        ]);
    }

    public function actionView($userId, $projectId)
    {
        return $this->render('view', [
            'model' => $this->findModel($userId, $projectId),
        ]);
    }

    public function actionCreate()
    {
        $model = new ProjectPartner();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'userId' => $model->userId, 'projectId' => $model->projectId]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($userId, $projectId)
    {
        $model = $this->findModel($userId, $projectId);
		
		//בדיקת הרשאה
		if (!\Yii::$app->user->can('updateProjectpartner', ['projectPartner' =>$model]) )
			throw new UnauthorizedHttpException('Hey, you are not allowed to update a partner for project that you not responsible for');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'userId' => $model->userId, 'projectId' => $model->projectId]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($userId, $projectId)
    {
		$model = $this->findModel($userId, $projectId);
		
		//בדיקת הרשאה
		if (!\Yii::$app->user->can('deleteProjectpartner', ['projectPartner' =>$model]) )
			throw new UnauthorizedHttpException('Hey, you are not allowed to update a partner for project that you not responsible for');
		
        $this->findModel($userId, $projectId)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($userId, $projectId)
    {
        if (($model = ProjectPartner::findOne(['userId' => $userId, 'projectId' => $projectId])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionDeletemultiple(){
       $action=Yii::$app->request->post('action');
       $selection=(array)Yii::$app->request->post('selection');
	   $userLogin = Yii::$app->user->identity->id;
	   if(!\Yii::$app->user->can('deleteUser')){
		   foreach($selection as $id){
			   $e = Projectpartner::findOne(['userId'=> $id]);
			   $taskpartner = Taskpartner::find()->all();
			   if (\Yii::$app->user->can('deleteProjectpartner')){
				   //מחיקת השותף גם ממשימות
				   foreach($taskpartner as $j){
						if($e->userId == $j->userId){
							$j->delete();
						}
					}
					$e->delete();
			   }
			   else{
				   $res = $e->projectItem->responsible;
				   if($userLogin == $res){
					   //מחיקת השותף גם ממשימות
					    foreach($taskpartner as $j){
							if($e->userId == $j->userId){
								$j->delete();
							}
						}
					   $e->delete();
				   }
			   }
		  }
	  }
	  else{
		  foreach($selection as $id){
			  $e = Projectpartner::findOne(['userId'=> $id]);
			  $taskpartner = Taskpartner::find()->all();
			  //מחיקת השותף גם ממשימות
			  foreach($taskpartner as $j){
				if($e->userId == $j->userId){
					$j->delete();
				}
			  }
			  $e->delete();
		  }
	  }
		
		return $this->redirect(['index']);
	}
}