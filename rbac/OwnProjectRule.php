<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnProjectRule extends Rule
{
	public $name = 'ownProjectRule';
	
	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['project']) ? $params['project']->responsible == $user : false;
		}
		return false;
	}
}