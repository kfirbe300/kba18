<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Task;

/* @var $this yii\web\View */
/* @var $model app\models\TaskPartner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-partner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userId')->dropDownList(User::getUsers()) ?>

    <?= $form->field($model, 'taskId')->dropDownList(Task::getTasks()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
